﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController))]
public class CharacterMovement : MonoBehaviour
{
    public float speed = 5f;
    public float jumpHeight = 2f;
    [Range(0.01f, 1f)]
    public float forwardJumpFactor = 0.05f;
    public float Gravity = -9.8f;
    public float DashFactor = 2f;
    public Vector3 Drag = new Vector3(1f, 2f, 1f);
    public float smoothTime = 0.15f;

    private CharacterController characterCotroller;
    private Vector3 moveDirection;
    private Vector3 smoothMoveDiection;
    private Vector3 smoother;
    private Vector3 horizontalVelocity;

    public bool isGrounded { get { return characterCotroller.isGrounded; } }
    public float currentSpeed { get { return horizontalVelocity.magnitude; } }
    public float currentNormalizedSpeed { get { return horizontalVelocity.normalized.magnitude; } }
    // Start is called before the first frame update
    void Start()
    {
        characterCotroller = GetComponent<CharacterController>();
    }

    public void moveCharacter(float hInput, float vInput, bool jump, bool dash)
    {
        float deltaTime = Time.deltaTime;
        float dashF = 1f;

        if (characterCotroller.isGrounded)
        {
            moveDirection = (hInput * transform.right + vInput * transform.forward).normalized;

            if (dash) dashF = DashFactor;
            if (jump)
            {
                if (Mathf.Abs(moveDirection.x) > 0f || Mathf.Abs(moveDirection.z) > 0f)
                {
                    moveDirection += moveDirection.normalized * (Mathf.Sqrt(jumpHeight * forwardJumpFactor * -Gravity / 2) * dashF);
                }
                moveDirection.y = Mathf.Sqrt(jumpHeight * -2f - Gravity);
            }
        }

        moveDirection.y += Gravity * deltaTime;

        moveDirection.x /= 1 + Drag.x * deltaTime;
        moveDirection.y /= 1 + Drag.y * deltaTime;
        moveDirection.z /= 1 + Drag.z * deltaTime;

        smoothMoveDiection = Vector3.SmoothDamp(smoothMoveDiection, moveDirection, ref smoother, smoothTime);

        smoothMoveDiection.y = moveDirection.y;

        characterCotroller.Move(smoothMoveDiection * (deltaTime * speed * dashF));

        horizontalVelocity.Set(characterCotroller.velocity.x, 0, characterCotroller.velocity.z);


    }

    // Update is called once per frame
    void Update()
    {

    }
}
