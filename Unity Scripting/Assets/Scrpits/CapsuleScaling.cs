using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;           //posibles ejes de escalado
    public float scaleUnit;         //velocidad de escalado

    //update is called once per frame
    void Update()
    {
        //Asociacion de los valores del escalado al valor unitario [-1,1]
        axes = CapsuleMovement.ClampVector3(axes);

        //la escala, al contrario de la rotacion y el movimiento, es acumulativa
        //lo que quiere decir que debemos a�adir el nuevo valor de la escala, al valor anterior.
        transform.localScale += axes * (scaleUnit * Time.deltaTime);
    }

}